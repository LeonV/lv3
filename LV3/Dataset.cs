﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class Dataset : Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public Dataset(Dataset dataset){
            data = new List<List<string>>();
            foreach (List<string> InnerList in dataset.GetData())
            {
                List<string> tempInnerList = new List<string>();
                foreach (string item in InnerList)
                {
                    string tempItem = item;
                    tempInnerList.Add(tempItem);
                }
                data.Add(tempInnerList);
            }
        }
        //Potrebno je duboko kopiranje zato što imamo listu listi stringova
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }
        public Prototype Clone()
        {
            Prototype dataClone = new Dataset(this);
            return dataClone;
        }
        public string printData()
        {
            StringBuilder builder = new StringBuilder();
            foreach (List<string> InnerList in this.data)
            {
                foreach (string item in InnerList)
                {
                    builder.Append(item);
                }
                builder.Append("\n");
            }
            return builder.ToString();
        }
    }
}
