﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\data.csv";
            Dataset dataSet = new Dataset(path);
            Dataset dataSetClone;
            dataSetClone = (Dataset)dataSet.Clone();
            Console.WriteLine(dataSet.printData());
            dataSet.ClearData();
            Console.WriteLine(dataSetClone.printData());
        }
    }
}
