﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;

        private Logger()
        {
            this.filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\log.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public string FilePath
        { 
            get { return this.filePath; } 
            set { this.filePath = value; } 
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
                {
                    writer.WriteLine(message);
                }
        }
        //Uporaba logera na vise mjesta u programu ce pisati u istu datoteku osim ako se promjeni filePath - tada se zapisuje u tu datoteku
    }
}
