﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification notification = new ConsoleNotification("Administrator", "Error", "Task failed successfully.", DateTime.Now, Category.ERROR, ConsoleColor.Red);
            NotificationManager manager = new NotificationManager();
            manager.Display(notification);
        }
    }
}
