﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            IBuilder builder = new NotificationBuilder();
            NotificationManager manager = new NotificationManager();
            manager.Display(builder.Build());
            builder.SetAuthor("Memory managment");
            builder.SetTitle("Critical Error!");
            builder.SetTime(DateTime.Now);
            builder.SetLevel(Category.ERROR);
            builder.SetColor(ConsoleColor.DarkRed);
            builder.SetText("Out of memory!");
            manager.Display(builder.Build());
        }
    }
}
