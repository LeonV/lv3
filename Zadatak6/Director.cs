﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class Director
    {
        IBuilder builder = new NotificationBuilder();
        public ConsoleNotification BuildNotificationERROR(String author)
        {
            return builder.SetAuthor(author).SetTitle("Error!").SetText("An error has occured!").SetColor(ConsoleColor.DarkRed).SetLevel(Category.ERROR).Build();
        }

        public ConsoleNotification BuildNotificationALERT(String author)
        {
            return builder.SetAuthor(author).SetTitle("Alert!").SetText("There was an alert.").SetColor(ConsoleColor.Yellow).SetLevel(Category.ALERT).Build();
        }

        public ConsoleNotification BuildNotificationINFO(String author)
        {
            return builder.SetAuthor(author).SetTitle("Information").SetText("Information text").SetColor(ConsoleColor.Cyan).SetLevel(Category.INFO).Build();
        }
    }
}
