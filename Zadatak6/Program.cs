﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager manager = new NotificationManager();
            Director director = new Director();
            manager.Display(director.BuildNotificationERROR("GPU"));
            manager.Display(director.BuildNotificationALERT("SystemAlertNotifications"));
            manager.Display(director.BuildNotificationINFO("SystemInformation"));
        }
    }
}
